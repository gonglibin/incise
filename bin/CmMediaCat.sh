 #!/usr/bin/env bash
 source /etc/profile
 HOME=$(cd "$(dirname "$0")/../"; pwd)
 cd $HOME
 mkdir -p log
 libjar=`ls lib | grep .jar | awk '{jar=jar"'"lib/"'"$1":"} END {print jar}'`
 CONF_DIR=${HOME}/config
 CP=${CONF_DIR}:$libjar
 MAIN_CLASS=com.charm.tools.CmMediaCat

 echo "java -cp ${CP}  ${MAIN_CLASS} $*"
 java -cp ${CP}  ${MAIN_CLASS} $*
