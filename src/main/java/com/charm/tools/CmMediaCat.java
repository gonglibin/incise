package com.charm.tools;

import java.util.List;
import java.util.Vector;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.charm.utils.CmAnsj;
import com.charm.utils.CmCats;
import com.charm.utils.CmConf;
import com.charm.utils.CmFast;
import com.charm.utils.CmMysql;
import com.charm.utils.CmCats.CmNode;

/**
 * 对媒体名称和描述进行分类模型匹配
 * @author gonglibin
 * 2017.10.16
 */

public class CmMediaCat {
	private static CmConf	cfg;
	private static CmMysql	sql;
	
	private static final String CM_COLN = ":";
	private static final String CM_MDDF = "UPDATE app_mediainfo SET type = 27 WHERE id = %d";
	private static final String CM_MDIF = "SELECT id, name, shortDescribe FROM app_mediainfo WHERE type = 0 AND platform LIKE 'wechat'";
	private static final String CM_MDID = "UPDATE app_mediainfo SET type = (SELECT id FROM tbl_com_media_type WHERE cmt_CtgyNos LIKE '%d' OR cmt_CtgyNos LIKE '%d,%%' OR cmt_CtgyNos LIKE '%%,%d,%%' OR cmt_CtgyNos LIKE '%%,%d') WHERE id = %d";

	/**
	 * 主函数
	 * @param 无
	 * @return 无
	 */
	public static void main(String[] args) {
		try {
			sql = new CmMysql();
			cfg = new CmConf(CmConf.CM_CSQL);
			sql.CmMysqlOpen(cfg.CmConfGetString(CmConf.CM_CURL), cfg.CmConfGetString(CmConf.CM_CUSR), cfg.CmConfGetString(CmConf.CM_CPWD));
			
			CmMediaCatWrite(CmMediaCatRead());
			
			sql.CmMysqlClose();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 从数据库中读取媒体描述信息
	 * @param 无
	 * @return 描述信息链表
	 */
	private static List<String> CmMediaCatRead() {
		List<String> lst = new ArrayList<String>();
		
		try {
			ResultSet res = sql.CmMysqlRstOpen(CM_MDIF);
			while (true == res.next()) {
				int mid = res.getInt(1);
				String nam = res.getString(2);
				String des = res.getString(3);
				if (nam.length() > 0 || des.length() > 0) {
					lst.add(mid + CM_COLN + nam + des);
				}
			}
			sql.CmMysqlRstClose();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return lst;
	}
	
	/**
	 * 识别分类并写入对应媒体分类ID
	 * @param l 描述信息链表
	 * @return 无
	 */
	private static void CmMediaCatWrite(List<String> l) {
		Vector<CmNode> nds = null;
		CmFast fst = new CmFast();
		CmCats cat = new CmCats(CmCats.CM_FCBS, CmCats.CM_FCMP);
		CmAnsj anj = new CmAnsj(CmAnsj.CM_CWDS, CmAnsj.CM_CNTS, CmAnsj.CM_CDIC);
		
		try {
			for (String s : l) {
				String[] arr = s.split(CM_COLN);
				anj.CmAnsjParse(arr[1].toString());
				String tag = fst.CmFastPredict(anj.CmAnsjToArray());
				if (null != (nds = cat.CmCatsGetMaps(fst.CmFastCategory(tag)))) {
					System.out.println(String.format(CM_MDID, nds.get(0).cid, nds.get(0).cid, nds.get(0).cid, nds.get(0).cid, new Integer(arr[0])));
					sql.CmMysqlExec(String.format(CM_MDID, nds.get(0).cid, nds.get(0).cid, nds.get(0).cid, nds.get(0).cid, new Integer(arr[0])));
				}
				else {
					System.out.println(String.format(CM_MDDF, new Integer(arr[0])));
					sql.CmMysqlExec(String.format(CM_MDDF, new Integer(arr[0])));
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}