package com.charm.utils;

import org.apache.commons.configuration.PropertiesConfiguration;

/**
 * 配置文件信息操作类对象
 * @author gonglibin
 * 2017.08.28
 */

public class CmConf {
	public static final String CM_CMDB = "dbs";
	public static final String CM_CHST = "host";
	public static final String CM_CPOT = "port";
	public static final String CM_CMTB = "table";
	
	public static final String CM_CURL = "MysqlUrl";
	public static final String CM_CUSR = "MysqlUser";
	public static final String CM_CPWD = "MysqlPswd";
	
	public static final String CM_CGID = "KafkaGroup";
	public static final String CM_CDTP = "KafkaDstTop";
	public static final String CM_CSTP = "KafkaSrcTop";
	public static final String CM_CKFK = "KafkaAddress";
	
	public static final String CM_CMGO = "./resources/mongo.properties";
	public static final String CM_CSQL = "./resources/mysql.properties";
	public static final String CM_CINC = "./resources/incise.properties";
	
	private PropertiesConfiguration cfg = null;
	
	/**
	 * 构造函数
	 * @param f 配置文件
	 * @return 无
	 */
	public CmConf() throws Exception {cfg = new PropertiesConfiguration(CM_CINC);}
	public CmConf(String f) throws Exception {cfg = new PropertiesConfiguration(f);}
	
	/**
	 * 获取键对应值
	 * @param k 键名称
	 * @return 键对应值
	 */
	public int CmConfGetInt(String k) {
		return cfg.getInt(k);
	}
	
	/**
	 * 获取键对应值
	 * @param k 键名称
	 * @return 键对应值
	 */
	public long CmConfGetLong(String k) {
		return cfg.getLong(k);
	}
	
	/**
	 * 获取键对应值
	 * @param k 键名称
	 * @return 键对应值
	 */
	public String CmConfGetString(String k) {
		return cfg.getString(k);
	}
	
	/**
	 * 设置键对应值
	 * @param k 键名称
	 * @param t	时间戳
	 * @return 无
	 */
	public void CmConfSetTime(String k, Long t) {
		cfg.setProperty(k, t);
	}
	
	/**
	 * 保存键值对
	 * @param 无
	 * @return 无
	 */
	public void CmConfSave() {
		try {
			cfg.save();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}