package com.charm.utils;

import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;

/**
 * MYSQL操作类用于数据库操作
 * @author gonglibin
 * 2017.10.16
 */

public class CmMysql {
	private Statement stat = null;
	private Connection conn = null;
	
	/**
	 * 打开到SQL的连接
	 * @param h 连接信息
	 * @param u 用户名
	 * @param p 密码
	 * @return 无
	 */
	public void CmMysqlOpen(String h, String u, String p) throws Exception {
		Class.forName("org.gjt.mm.mysql.Driver").newInstance();
		conn = DriverManager.getConnection(h, u, p);
	}
	
	/**
	 * 关闭连接对象
	 * @param 无
	 * @return 无
	 */
	public void CmMysqlClose() throws Exception {
		conn.close();
		conn = null;
	}
	
	/**
	 * 提交SQL执行（用于查询）
	 * @param s SQL命令
	 * @return ResultSet对象
	 */
	public ResultSet CmMysqlRstOpen(String s) throws Exception {
		stat = conn.createStatement();

		return stat.executeQuery(s);
	}
	
	/**
	 * 关闭Statement对象
	 * @param 无
	 * @return 无
	 */
	public void CmMysqlRstClose() throws Exception {
		stat.close();
		stat = null;
	}
	
	/**
	 * 指令回滚
	 * @param 无
	 * @return 无
	 */
	public void CmMysqlRoll() throws Exception {
		conn.rollback();
	}
	
	/**
	 * 指令提交
	 * @param 无
	 * @return 无
	 */
	public void CmMysqlCommit() throws Exception {
		conn.commit();
	}
	
	/**
	 * 提交SQL执行（用于非查询）
	 * @param SQL命令
	 * @return 无
	 */
	public void CmMysqlExec(String s) throws Exception {
		conn.createStatement().execute(s);
	}
}