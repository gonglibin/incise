package com.charm.utils;

import java.util.List;
import com.charm.fsttxt.Pair;
import com.charm.fsttxt.FastText;

/**
 * 脸书开源快文对象用于文章类目识别
 * @author gonglibin
 * 2017.09.20
 */

public class CmFast {
	private FastText fst = new FastText();
	
	public static final String CM_CTAG = "__label__";
	public static final String CM_FAST = "./config/CmFastTextBin.dat";
	
	/**
	 * CmFast构造函数
	 * @param f 数据模型文件
	 * @return 无
	 */
	public CmFast() {
		try {
			fst.loadModel(CM_FAST);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public CmFast(String f) {
		try {
			fst.loadModel(f);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 识别批量文章分类
	 * @param s 正文数组
	 * @return 分类及权重
	 */
	public String CmFastPredict(String[] s) {
		return fst.predict(s, 1).get(0).getValue();
	}
	
	/**
	 * 识别批量文章分类
	 * @param s 正文数组
	 * @param n 类目个数
	 * @return 分类权重链表
	 */
	public List<Pair<Float, String>> CmFastPredict(String[] s, int n) {
		return fst.predict(s, n);
	}
	
	/**
	 * 获取分类名称
	 * @param c 含标签分类
	 * @return 不含标签分类
	 */
	public String CmFastCategory(String c) {
		return c.substring(CM_CTAG.length());
	}
}