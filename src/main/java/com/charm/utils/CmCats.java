package com.charm.utils;

import java.util.Map;
import java.util.Vector;
import java.util.TreeMap;
import java.util.HashMap;
import java.io.FileReader;
import java.io.BufferedReader;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**
 * 一级类目为key，二级类目ID为vector
 * @author gonglibin
 * 2017.09.11
 */

/**
 * news_society			社会
 * news_politics		时政
 * business				商业
 * news_finance			财经
 * stock				股票
 * news_military		军事
 * news_sports			体育
 * news_world			国际
 * news_fashion			时尚
 * news_health			健康
 * news_regimen			养生
 * emotion				情感
 * news_psychology		心理
 * news_baby			育儿
 * news_travel			旅行
 * news_food			美食
 * news_photography		摄影
 * news_entertainment	娱乐
 * movie				电影
 * news_geomantic		风水
 * news_astrology		星座
 * news_comic			动漫
 * news_game			游戏
 * funny				搞笑
 * news_lieqi			猎奇
 * science_all			科学
 * news_tech			科技
 * digital				数码
 * news_career			职场
 * news_edu				教育
 * news_essay			美文
 * news_history			历史
 * news_novel			小说
 * news_culture			文化
 * news_collect			收藏
 * buddhism				佛教
 * news_house			房产
 * news_car				汽车
 * news_home			家居
 * news_pet				宠物
 * lottery				彩票
 * immigration			移民
 * news_agriculture		三农
 * news_story			故事
 * news_law				法制
 * news_design			设计
 * news_beauty			美女
 * cellphone			手机
 */

public class CmCats {
	/**
	 * 类目与其ID对应结构类
	 * cat 分类名称
	 * cid 分类的ID
	 */
	public static class CmNode {
		public String	cat;
		public Integer	cid;
		
		public CmNode() {}
		public CmNode(String c, Integer i) {cat = c; cid = i;}
	}
	
	/**
	 * 配置文件转换对象
	 * src 基础分类
	 * dst 映射分类集合
	 */
	public static class CmMaps {
		public String	src;
		public String[]	dst;
		
		public CmMaps() {}
		public CmMaps(String s, String[] d) {src = s; dst = d;}
		
		/**
		 * 数据反序列化
		 * @param s json字符串
		 * @return CmTows对象
		 */
		public static CmMaps CmMappDeserialize(String s) {
			return JSON.parseObject(s, CmMaps.class);
		}
		
		/**
		 * 数据序列化
		 * @param s 聚类类目
		 * @param d 目标类目
		 * @return JSON字符串
		 */
		public static String CmMapsSerialize(String s, String[] d) {
			JSONObject jsn = new JSONObject();
			
			jsn.put("src", s);
			jsn.put("dst", d);
			
			return JSON.toJSONString(jsn);
		}
	}
	
	private TreeMap<String, Vector<CmNode>> mps = new TreeMap<>();
	public static final String	CM_FCBS = "./config/CmBasCategory.dat";
	public static final String	CM_FCMP = "./config/CmMapCategory.dat";
	
	/**
	 * CmCats构造函数
	 * @param b 基础分类文件
	 * @param m 映射分类文件
	 * @return 无
	 */
	public CmCats() {}
	public CmCats(String b, String m) {
		Integer idx = 1;
		String buf = new String();
		BufferedReader brd = null;
		Map<String, Integer> cat = new HashMap<>();
		
		try {
			/* 加载基础分类 */
			brd = new BufferedReader(new FileReader(b));
			while (null != (buf = brd.readLine())) {
				cat.put(buf.trim(), idx ++);
			}
			brd.close();
			
			/* 加载映射关系 */
			brd = new BufferedReader(new FileReader(m));
			while (null != (buf = brd.readLine())) {
				Vector<CmNode> vec = new Vector<CmNode>();
				CmMaps mpp = CmMaps.CmMappDeserialize(buf);
				for (String d : mpp.dst) {
					vec.add(new CmNode(d, cat.get(d)));
				}
				mps.put(mpp.src, vec);
			}
			brd.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 从词库类目中查询映射关系集合
	 * @param k 词库一级类目名称
	 * @return 系统一级类目名称集合
	 */
	public Vector<CmNode> CmCatsGetMaps(String k) {
		return mps.get(k);
	}
	
	/**
	 * 打印Map<String, Vector<CmNode>>数据
	 * @param 无
	 * @return 无
	 */
	public void CmCatsGetMapsShow() {
		for (Map.Entry<String, Vector<CmNode>> v : mps.entrySet()) {
			System.out.print(v.getKey() + " -> ");
			for (CmNode c : v.getValue()) {
				System.out.print(c.cid + "[" + c.cat + "], ");
			}
			System.out.println();
		}
	}
}