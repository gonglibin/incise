package com.charm.utils;

import java.util.List;
import java.util.ArrayList;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.bson.conversions.Bson;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.model.Filters;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoCollection;

/**
 * mongo数据库操作类
 * @author gonglibin
 * 2017.11.28
 */

public class CmMongo {
	private MongoClient mc;
	private MongoDatabase db;
	private MongoCollection<Document> dc;
	
	/**
     * 获取MongoClient对象
     * @param 无
     * @return MongoClient对象
     */
    public MongoClient CmMongoClient() {
        return mc;
    }
    
    /**
     * 获取MongoDatabase对象
     * @param 无
     * @return MongoDatabase对象
     */
    public MongoDatabase CmMongoDatabase() {
        return db;
    }
    
    /**
     * 获取MongoCollection对象
     * @param 无
     * @return MongoCollection对象
     */
    public MongoCollection<Document> CmMongoCollection() {
        return dc;
    }
    
    /**
     * 创建MongoClient对象
     * @param h 数据库地址
     * @param p 数据库端口
     * @return 无
     */
    public void CmMongoOpen(String h, int p) {
        mc = new MongoClient(h, p);
    }
    
    /**
     * 关闭MongoClient对象
     * @param 无
     * @return 无
     */
    public void CmMongoClose() {
        mc.close();
    }
    
    /**
     * 获取库句柄
     * @param d 数据库名称
     * @return 无
     */
    public void CmMongoGetDatabase(String d) {
        db = mc.getDatabase(d);
    }
    
    /**
     * 获取数据集句柄
     * @param c 数据集名称
     * @return 无
     */
    public void CmMongoGetCollection(String c) {
        dc = db.getCollection(c);
    }
    
    /**
     * 插入文档记录
     * @param d 文档对象
     * @return 无
     */
    public void CmMongoInsert(Document d) {
        dc.insertOne(d);
    }
    
    /**
     * 遍历全量记录
     * @param 无
     * @return Document对象迭代器
     */
    public FindIterable<Document> CmMongoSelect() {
    	return dc.find();
    }
    
    /**
     * 检索并返回指定主键的记录
     * @param b 预生成过滤器
     * @return Document对象迭代器
     */
    public FindIterable<Document> CmMongoSelect(Bson b) {
    	return dc.find(b);
    }
    
    /**
     * 检索并返回指定主键的记录
     * @param k 字段名
     * @param v 字段值
     * @return Document对象迭代器
     */
    public FindIterable<Document> CmMongoSelect(String k, String v) {
    	List<Bson> flt = new ArrayList<Bson>();
    	
    	flt.add(Filters.in(k, v));
    	
    	return dc.find(Filters.and(flt));
    }
    
    /**
     * 检索并返回指定_id的记录
     * @param k 字段名
     * @param v ObjectId对象
     * @return Document对象迭代器
     */
    public FindIterable<Document> CmMongoSelect(String k, ObjectId v) {
    	List<Bson> flt = new ArrayList<Bson>();
    	
    	flt.add(Filters.in(k, v));
    	
    	return dc.find(Filters.and(flt));
    }
    
    /**
     * 删除指定主键的记录
     * @param k 字段名
     * @param v 字段值
     * @return 无
     */
    public void CmMongoDelete(String k, String v) {
    	dc.deleteOne(Filters.in(k, v));
    }
    
    /**
     * 删除指定主键的记录
     * @param k 字段名
     * @param v ObjectId对象
     * @return 无
     */
    public void CmMongoDelete(String k, ObjectId v) {
    	dc.deleteOne(Filters.in(k, v));
    }
}