package com.charm.incise;

import java.sql.ResultSet;
import com.charm.utils.CmMysql;

public class TestCmMysql {
	public static void main(String[] args) {
		CmMysql db = new CmMysql();
		String cn = "jdbc:mysql://192.168.0.7/sunlake";

		try {
			db.CmMysqlOpen(cn, "root", "123456");
//			db.CmMysqlExec("UPDATE app_mediainfo SET type = (SELECT id FROM tbl_com_media_type WHERE cmt_CtgyNos LIKE '%12%') WHERE id = 3739");
			ResultSet res = db.CmMysqlRstOpen("SELECT id, name, shortDescribe FROM app_mediainfo");
			while (res.next()) {
				String nam = res.getString("name");
				String des = res.getString("shortDescribe");
				if (nam.length() > 0 && des.length() > 0) {
					System.out.println(res.getInt("id") + ": {name: " + nam + " -> " + "Desc: " + des + "}");
				}
			}
			db.CmMysqlRstClose();
			db.CmMysqlClose();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}