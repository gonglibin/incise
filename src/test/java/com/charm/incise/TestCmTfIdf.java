package com.charm.incise;

import java.util.Arrays;
import com.charm.utils.CmTfIdf;
import com.charm.utils.CmAnsj.CmWord;

/**
 * tf-idf测试用例并输出结果
 * @author gonglibin
 * 2017.08.22
 */

public class TestCmTfIdf {
	public static void main(String[] args) {
		CmWord[] wd = {
			new CmWord(0, "五月", "t"),
			new CmWord(2, "天上", "s"),
			new CmWord(4, "周日", "t"),
			new CmWord(7, "台北", "ns"),
			new CmWord(10, "巨", "ag"),
			new CmWord(11, "蛋", "n"),
			new CmWord(12, "热力", "n"),
			new CmWord(14, "开", "v"),
			new CmWord(15, "唱", "v"),
			new CmWord(17, "五月", "t"),
			new CmWord(19, "天", "q"),
			new CmWord(21, "演唱会", "n"),
			new CmWord(24, "向来", "d"),
			new CmWord(28, "难", "a"),
			new CmWord(29, "求", "v"),
			new CmWord(32, "周六", "t"),
			new CmWord(35, "日", "j"),
			new CmWord(39, "台北", "ns"),
			new CmWord(42, "巨", "ag"),
			new CmWord(43, "蛋", "n"),
			new CmWord(44, "开", "v"),
			new CmWord(45, "唱", "v"),
			new CmWord(48, "网友", "n"),
			new CmWord(51, "媒体", "n"),
			new CmWord(53, "投诉", "vn"),
			new CmWord(56, "当天", "t"),
			new CmWord(60, "粉丝", "n"),
			new CmWord(63, "工作", "vn"),
			new CmWord(65, "人员", "n"),
			new CmWord(69, "争议", "vn"),
			new CmWord(71, "座位", "n"),
			new CmWord(74, "为由", "v"),
			new CmWord(77, "挡", "v"),
			new CmWord(79, "门外", "s"),
			new CmWord(86, "内地", "s"),
			new CmWord(88, "粉丝", "n"),
			new CmWord(92, "当天", "t"),
			new CmWord(95, "台湾", "ns"),
			new CmWord(100, "进场", "v"),
			new CmWord(102, "时", "ng"),
			new CmWord(105, "拦", "v"),
			new CmWord(112, "找", "v"),
			new CmWord(113, "消", "v"),
			new CmWord(114, "保", "v"),
			new CmWord(115, "官", "n"),
			new CmWord(116, "申诉", "v"),
			new CmWord(119, "只能", "v"),
			new CmWord(122, "场", "q"),
			new CmWord(123, "外", "f"),
			new CmWord(124, "痛哭", "v"),
			new CmWord(129, "五月", "t"),
			new CmWord(131, "天", "q"),
			new CmWord(132, "所属", "n"),
			new CmWord(136, "音乐", "n"),
			new CmWord(138, "违反", "v"),
			new CmWord(140, "艺", "ng"),
			new CmWord(141, "文", "ng"),
			new CmWord(142, "活动", "vn"),
			new CmWord(144, "定型", "v"),
			new CmWord(146, "化", "v"),
			new CmWord(147, "契约", "n"),
			new CmWord(153, "对此", "d"),
			new CmWord(158, "音乐", "n"),
			new CmWord(165, "售票", "vn"),
			new CmWord(167, "前", "f"),
			new CmWord(169, "公告", "n"),
			new CmWord(171, "相关", "v"),
			new CmWord(173, "售票", "vn"),
			new CmWord(176, "入场", "v"),
			new CmWord(181, "并未", "d"),
			new CmWord(183, "事先", "d"),
			new CmWord(185, "揭露", "v"),
			new CmWord(190, "导致", "v"),
			new CmWord(192, "消费者", "n"),
			new CmWord(199, "误解", "n"),
			new CmWord(202, "疑虑", "n"),
			new CmWord(206, "打击", "v"),
			new CmWord(208, "不法", "b"),
			new CmWord(210, "黄牛", "n"),
			new CmWord(213, "维护", "v"),
			new CmWord(217, "消费者", "n"),
			new CmWord(220, "权益", "n"),
			new CmWord(223, "主办", "v"),
			new CmWord(225, "单位", "n"),
			new CmWord(227, "事前", "t"),
			new CmWord(229, "不厌其烦", "i"),
			new CmWord(233, "提醒", "v"),
			new CmWord(235, "歌迷", "n"),
			new CmWord(238, "买", "v"),
			new CmWord(239, "来路不明", "i"),
			new CmWord(244, "票", "n"),
			new CmWord(246, "争议", "vn"),
			new CmWord(248, "票", "n"),
			new CmWord(249, "券", "ng"),
			new CmWord(254, "民众", "n"),
			new CmWord(256, "检举", "v"),
			new CmWord(261, "查核", "v"),
			new CmWord(263, "确实", "ad"),
			new CmWord(265, "加价", "v"),
			new CmWord(267, "哄抬", "v"),
			new CmWord(270, "不法", "b"),
			new CmWord(278, "音乐", "n"),
			new CmWord(285, "前", "f"),
			new CmWord(286, "陆续", "d"),
			new CmWord(289, "官方", "n"),
			new CmWord(291, "脸", "n"),
			new CmWord(292, "书", "n"),
			new CmWord(293, "公布", "v"),
			new CmWord(295, "争议", "vn"),
			new CmWord(297, "票", "n"),
			new CmWord(298, "券", "ng"),
			new CmWord(299, "名单", "n"),
			new CmWord(303, "接受", "v"),
			new CmWord(305, "退票", "v"),
			new CmWord(307, "申请", "v"),
			new CmWord(317, "争议", "vn"),
			new CmWord(323, "配合", "v"),
			new CmWord(327, "消费", "vn"),
			new CmWord(329, "纠纷", "n"),
			new CmWord(331, "厘", "q"),
			new CmWord(332, "清", "a"),
			new CmWord(334, "调解", "v"),
			new CmWord(338, "五月", "t"),
			new CmWord(340, "天", "q"),
			new CmWord(343, "重返", "v"),
			new CmWord(345, "台北", "ns"),
			new CmWord(348, "巨", "ag"),
			new CmWord(349, "蛋", "n"),
			new CmWord(352, "门票", "n"),
			new CmWord(354, "早已", "d"),
			new CmWord(356, "完", "v"),
			new CmWord(357, "售", "v"),
			new CmWord(359, "今晚", "t"),
			new CmWord(366, "演出", "v"),
			new CmWord(372, "买", "v"),
			new CmWord(374, "票", "n"),
			new CmWord(377, "迷", "v"),
			new CmWord(379, "烦恼", "an"),
			new CmWord(382, "农历", "n"),
			new CmWord(384, "年", "q"),
			new CmWord(385, "前", "f"),
			new CmWord(392, "五月", "t"),
			new CmWord(394, "天", "q"),
			new CmWord(396, "机会", "n"),
			new CmWord(409, "风云", "n"),
			new CmWord(411, "榜", "n"),
			new CmWord(414, "明年", "t"),
			new CmWord(421, "嗨", "e"),
			new CmWord(422, "爆", "v"),
			new CmWord(423, "台北", "ns"),
			new CmWord(426, "巨", "ag"),
			new CmWord(427, "蛋", "n"),
			new CmWord(429, "日前", "t"),
			new CmWord(431, "公布", "v"),
			new CmWord(435, "波演唱嘉", "nr"),
			new CmWord(440, "田馥甄", "nr"),
			new CmWord(448, "公布", "v"),
			new CmWord(450, "超", "v"),
			new CmWord(451, "强", "a"),
			new CmWord(452, "卡", "n"),
			new CmWord(453, "司", "ng"),
			new CmWord(484, "最终", "d"),
			new CmWord(486, "章", "q"),
			new CmWord(488, "自传", "n"),
			new CmWord(490, "复", "dg"),
			new CmWord(491, "刻", "v"),
			new CmWord(492, "版", "n"),
			new CmWord(494, "演唱会", "n"),
			new CmWord(498, "五月", "t"),
			new CmWord(500, "天", "q"),
			new CmWord(509, "脸", "n"),
			new CmWord(510, "书", "n"),
			new CmWord(512, "出题", "v"),
			new CmWord(518, "单车", "n"),
			new CmWord(523, "咖啡", "n"),
			new CmWord(532, "眼镜", "n"),
			new CmWord(543, "提示", "v"),
			new CmWord(549, "粉丝", "n"),
			new CmWord(554, "猜中", "v"),
			new CmWord(557, "五月", "t"),
			new CmWord(559, "天", "q"),
			new CmWord(562, "出题", "v"),
			new CmWord(564, "人员", "n"),
			new CmWord(567, "受", "v"),
			new CmWord(568, "打击", "v"),
			new CmWord(572, "声明", "n"),
			new CmWord(575, "中华", "nz"),
			new CmWord(577, "娱乐", "vn"),
			new CmWord(579, "网", "n"),
			new CmWord(580, "刊载", "v"),
			new CmWord(582, "此文", "r"),
			new CmWord(586, "传递", "v"),
			new CmWord(590, "信息", "n"),
			new CmWord(593, "目的", "n"),
			new CmWord(598, "意味着", "v"),
			new CmWord(601, "赞同", "v"),
			new CmWord(604, "观点", "n"),
			new CmWord(607, "证实", "v"),
			new CmWord(610, "描述", "v"),
			new CmWord(613, "版权", "n"),
			new CmWord(616, "作者", "n"),
			new CmWord(622, "同类", "n"),
			new CmWord(624, "文章", "n"),
			new CmWord(626, "敬请", "v"),
			new CmWord(628, "浏览", "vn"),
			new CmWord(631, "音乐", "n"),
			new CmWord(633, "天地", "n"),
		};
		CmTfIdf ti = new CmTfIdf(CmTfIdf.CM_FIDF);
		ti.CmTfIdfPrint(ti.CmTfIdfTurn(Arrays.asList(wd), 0), 20);
	}
}