package com.charm.incise;

import com.charm.fsttxt.Pair;
import com.charm.utils.CmFast;

/**
 * 对内容分类模块进行单元测试
 * @author gonglibin
 * 2017.08.30
 */

public class TestCmFast {
	public static void main(String[] args) {
		CmFast fst = new CmFast();
		String sen =
			"海外 网 9月 16日 电 香港 立法会 议员 热血 公民 成员 郑松泰 控 侮辱 国旗 罪 侮辱 " +
			"区旗 罪 案件 结案 陈词 法庭 案件 押 本月 29日 裁决 郑松泰 获准 保释 香港 文汇 网 " +
			"15日 消息 裁判 官郑念 慈 本案 关键 分歧 实质 弄 污 损毁 国旗 区 旗 法庭 接纳 毋 须 " +
			"实质 弄 污 损毁 国旗 区 旗 侮辱 国旗 区旗 辩 说 立法会 流 郑松泰 吸引 议会 返回 议事 " +
			"厅 裁判 官 反 问 吸引 说 肚 饿 偷窃 法庭 目的 去年 10月 19日 首次 宣誓 时 作出 辱 国 " +
			"举动 两名 候 议员 获 立法会 主席 准许 宣誓 期间 郑松泰 径自 座 走 建制 派 议员 座位 " +
			"摆放 头 国旗 区旗 拔 倒插 控 侮辱 国旗 区旗 罪 9月 7日 控 侮辱 国旗 侮辱 区旗 两 罪 " +
			"立法会 议员 郑松泰 东区 法院 裁定 侮辱 国旗 区旗 罪 表 证 成立 案件 15日 结案 陈 词 " +
			"控 指 被告 损害 国家 香港 尊严";
		String[] wds = sen.split(" ");
		for (Pair<Float, String> l : fst.CmFastPredict(wds, 5)) {
			System.out.println(l.getValue() + ": " + Math.exp(l.getKey()));
		}
	}
}