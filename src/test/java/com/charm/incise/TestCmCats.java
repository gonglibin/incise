package com.charm.incise;

import com.charm.utils.CmCats;

/**
 * 测试聚类类目与昌荣类目的映射关系
 * @author gonglibin
 * 2017.08.30
 */

public class TestCmCats {
	public static void main(String[] args) {
		CmCats cat = new CmCats(CmCats.CM_FCBS, CmCats.CM_FCMP);
		cat.CmCatsGetMapsShow();
	}
}