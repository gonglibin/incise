package com.charm.incise;

import com.charm.utils.CmConf;

/**
 * 测试properties配置文件读写
 * @author gonglibin
 * 2017.08.28
 */

public class TestCmConf {
	public static void main(String[] args) {
		try {
			CmConf cfg = new CmConf();
			System.out.println("KafkaGroup: " + cfg.CmConfGetString("KafkaGroup"));
			System.out.println("KafkaDstTop: " + cfg.CmConfGetString("KafkaDstTop"));
			System.out.println("KafkaSrcTop: " + cfg.CmConfGetString("KafkaSrcTop"));
			System.out.println("KafkaAddress: " + cfg.CmConfGetString("KafkaAddress"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}