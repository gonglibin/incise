package com.charm.incise;

import com.charm.utils.CmKafka;

/**
 * 总线消息消费者测试用例
 * @author gonglibin
 * 2017.08.30
 */

public class TestCmCons {
	public static void main(String[] args) {
		int idx = 0;
		CmKafka ckc = new CmKafka(CmKafka.CM_CONS, "incise", "t_gonglibin_text", "192.168.0.7:9092");
		
		ckc.CmKafkaConsSeek(new int[]{0, 1, 2}, 0L);
		while (true) {
			for (String s : ckc.CmKafkaConsumer(CmKafka.CM_TOUT)) {
				System.out.println(++ idx + ": " + s.subSequence(0, 10));
			}
			ckc.CmKafkaConsCommit();
		}
		
//		ckc.CmKafkaClose();
	}
}