package com.charm.incise;

import org.bson.Document;
import com.charm.utils.CmMongo;

public class TestCmMongo {
	private static final String CM_NMDB = "sample";
	private static final String CM_NMTB = "article";
	
	public static void main(String[] args) {
		CmMongo cm = new CmMongo();
		
		try {
			cm.CmMongoOpen("192.168.0.7", 27017);
			cm.CmMongoGetDatabase(TestCmMongo.CM_NMDB);
			cm.CmMongoGetCollection(TestCmMongo.CM_NMTB);
			
			for (Document d : cm.CmMongoSelect().limit(3)) {
				System.out.println("title: " + d.getString("title") + " -> " + "category: " + d.getString("category"));
				System.out.println("content: " + d.getString("content").replace('\n', ' ').substring(0, 30) + " ......");
//				System.out.println(d.getString("title") + d.getString("content") + "\t__label__" + d.getString("category"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			cm.CmMongoClose();
		}
	}
}