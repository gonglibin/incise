package com.charm.incise;

import java.util.List;
import com.charm.fsttxt.Pair;
import com.charm.utils.CmFast;
import com.charm.utils.CmAnsj;
import com.charm.utils.CmAnsj.CmWord;

/**
 * 对文章分词再匹配分类模型
 * @author gonglibin
 * 2017.10.10
 */

public class TestCmFtxt {
	public static void main(String[] args) {
		CmFast fst = new CmFast();
		CmAnsj anj = new CmAnsj(CmAnsj.CM_CWDS, CmAnsj.CM_CNTS, CmAnsj.CM_CDIC);
		List<CmWord> lst = anj.CmAnsjParse("据中国之声《新闻和报纸摘要》报道，“金九银十”向来是楼市传统的销售旺季，但是在今年国庆长假期间，热点城市楼市明显遇冷。数据显示，30个城市网签大幅下跌，平均跌幅达到八成成交量创近三年来最低。 中原地产的统计数据显示，黄金周前6天，楼市热点城市出现了明显下调，北京新建住宅前6日网签78套，二手房网签23套，均是2009年来的历史最低值，同比跌幅达到了72%，上海前6日网签新建住宅147套，同比跌幅达到了78%，在广州，开发商总推货量和总成交货量均比去年十一假期大跌七成左右。 中原地产首席分析师张大伟：一方面原因的话是这个楼市政策的持续发力，其实从5月份开始15个核心热点城市就已经全部进入了一个下行的周期，第二个原因应该说也跟去年的前值相对比较高有关系，平均的跌幅就在这个2017年的这个长假中体现的非常明显。 这种成交下跌，也是今年整个市场行情的延续，根据1-9月统计数据，北京、上海、广州、深圳等一线城市，平均成交跌幅达到了40%，二线城市之前的热点城市，南京下调了57%，福州下调60%。 上海易居房地产研究院总监严跃进：比如像北京、上海等城市，现在无论普通住房还是酒店式公寓，都是很严厉的。另外跟信贷政策收紧也有关。 张大伟认为，楼市>交易方面的各种约束性政策的持续出台，让这一轮楼市调控效果更为明显。 张大伟：不光是像往年的调控一样做好减法，抑制了投资投机需求，同时也增加了大量土地供给，做好了加法，应该说这一轮调控和往年的短效机制相比，已经有了一些长效机制的苗头。");
		
		int idx = 0;
		String[] wds = new String[lst.size()];
		for (CmWord w : lst) wds[idx ++] = w.val;
		
		for (Pair<Float, String> l : fst.CmFastPredict(wds, 5)) {
			System.out.println(l.getValue() + ": " + Math.exp(l.getKey()));
		}
	}
}