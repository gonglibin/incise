package com.charm.incise;

import java.util.List;
import java.util.ArrayList;
import com.charm.utils.CmKafka;

/**
 * 总线消息生产者测试用例
 * @author gonglibin
 * 2017.08.30
 */

public class TestCmProd {
	public static void main(String[] args) {
		CmKafka ckp = new CmKafka(CmKafka.CM_PROD, null, "t_gonglibin_text", "192.168.0.7:9092");
		List<String> lst = new ArrayList<String>();
		
		lst.add("{\"url\":\"www.url1.com\",\"title\":\"标题1\",\"content\":\"正文1\"}");
		lst.add("{\"url\":\"www.url2.com\",\"title\":\"标题2\",\"content\":\"正文2\"}");
		lst.add("{\"url\":\"www.url3.com\",\"title\":\"标题3\",\"content\":\"正文3\"}");
		lst.add("{\"url\":\"www.url4.com\",\"title\":\"标题4\",\"content\":\"正文4\"}");
		lst.add("{\"url\":\"www.url5.com\",\"title\":\"标题5\",\"content\":\"正文5\"}");
		
		ckp.CmKafkaProducer(lst);
		ckp.CmKafkaClose();
	}
}